package com.apuntesdejava.arquillian.exception;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
public class OutStockException extends Exception {

    private static final long serialVersionUID = 5903457840918966892L;

    /**
     * Creates a new instance of <code>OutStockExcepction</code> without detail
     * message.
     */
    public OutStockException() {
    }

    /**
     * Constructs an instance of <code>OutStockExcepction</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public OutStockException(String msg) {
        super(msg);
    }
}
