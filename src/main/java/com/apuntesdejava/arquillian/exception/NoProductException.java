package com.apuntesdejava.arquillian.exception;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
public class NoProductException extends Exception {

    private static final long serialVersionUID = -8745023967899450233L;

    /**
     * Creates a new instance of <code>NoProductException</code> without detail
     * message.
     */
    public NoProductException() {
    }

    /**
     * Constructs an instance of <code>NoProductException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoProductException(String msg) {
        super(msg);
    }
}
