package com.apuntesdejava.arquillian.rest;

import com.apuntesdejava.arquillian.exception.NoProductException;
import com.apuntesdejava.arquillian.exception.OutStockException;
import com.apuntesdejava.arquillian.logic.ProductFacade;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Path("sell")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class SellREST {

    @Inject
    private ProductFacade productFacade;

    @POST
    @Produces(MediaType.TEXT_PLAIN)

    public Response sell(
            @FormParam("productId") long productId,
            @FormParam("quantity") int quantity
    ) {
        try {
            double mount = productFacade.sell(productId, quantity);
            return Response.ok(mount).build();
        } catch (NoProductException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (OutStockException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
