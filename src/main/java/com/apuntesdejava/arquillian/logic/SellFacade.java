package com.apuntesdejava.arquillian.logic;

import com.apuntesdejava.arquillian.domain.Product;
import com.apuntesdejava.arquillian.domain.Sell;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Stateless
public class SellFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellFacade.class);

    @PersistenceContext(unitName = "storePU")
    private EntityManager em;

    public Sell saveSell(Product p, int quantity) {
        LOGGER.debug("vender producto: {} \t cantidad:{}", p, quantity);
        Sell sell = new Sell();
        sell.setProduct(p);
        sell.setQuantity(quantity);
        sell.setMount(quantity * p.getPrice());
        sell.setSaleDate(new Date());
        em.persist(sell);
        return sell;
    }
}
