package com.apuntesdejava.arquillian.logic;

import com.apuntesdejava.arquillian.domain.Product;
import com.apuntesdejava.arquillian.domain.Sell;
import com.apuntesdejava.arquillian.exception.NoProductException;
import com.apuntesdejava.arquillian.exception.OutStockException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Stateless
public class ProductFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductFacade.class);

    @PersistenceContext(unitName = "storePU")
    private EntityManager em;

    @Inject
    private SellFacade sellFacade;

    public Product create(String name, int stock, double price) {
        Product p = new Product(name, stock, price);
        em.persist(p);
        return p;
    }

    public List<Product> findByName(String name) {
        LOGGER.debug("buscando por nombre:{}", name);
        String hint = '%' + name.toUpperCase().trim().replaceAll(" ", "%") + '%';
        TypedQuery<Product> query = em.createQuery("select p from Product p where UPPER( p.name ) like :name", Product.class)
                .setParameter("name", hint);
        return query.getResultList();
    }

    public double sell(Long productId, int quantity) throws OutStockException, NoProductException {
        //busca un producto
        Product prod = em.find(Product.class, productId);

        if (prod == null) { //si no encontró...
            throw new NoProductException(); //... lanza la excepcion
        }
        prod.setStock(prod.getStock() - quantity); //descuenta el stock
        if (prod.getStock() < 0) { // si llega al negativo...
            throw new OutStockException(); //... lanza la exception
        }
        em.merge(prod); //actualiza la base de datos
        //registra la venta
        Sell sell = sellFacade.saveSell(prod, quantity);
        //devuelve el precio total del producto vendido
        return sell.getMount();
    }

}
